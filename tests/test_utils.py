import colander

from pyramid.httpexceptions import HTTPBadRequest, HTTPOk
from pytest import fixture

from app.utils import match_schema
from app.schemas import StringList


class MockRequest(object):
    pass


@fixture
def empty_request():
    request = MockRequest()
    request.POST = {}
    request.method = 'POST'
    return request


@fixture
def named_request():
    request = MockRequest()
    request.POST = {'name': 'Test'}
    request.method = 'POST'
    return request


class Dummy(colander.MappingSchema):
    name = colander.SchemaNode(colander.String(allow_empty=False))


class DummyView(object):

    def __init__(self, request):
        self.request = request

    @match_schema(Dummy)
    def dummy(self, obj=None):
        return HTTPOk()


class TestSchemaMatcher(object):

    def test_dummy_wo_data(self, empty_request):
        view = DummyView(empty_request)
        result = view.dummy()

        assert isinstance(result, HTTPBadRequest)

    def test_dummy_w_data(self, named_request):
        view = DummyView(named_request)
        result = view.dummy()

        assert isinstance(result, HTTPOk)


class TestCustomSchemas(object):

    def test_string_list(self):
        class TestSchema(colander.MappingSchema):
            ids = colander.SchemaNode(StringList())

        schema = TestSchema()
        obj = schema.deserialize({"ids": "1,2,3"})
        assert obj['ids'] == [u'1', u'2', u'3']

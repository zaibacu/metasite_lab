import click
import colander

from functools import wraps

from dotenv import load_dotenv, find_dotenv
from pyramid.view import view_config
from pyramid.paster import get_appsettings, setup_logging
from pyramid.httpexceptions import HTTPBadRequest


def admin_view(**kwargs):
    """
    Same as normal view, just with some helpers
    """
    return view_config(permission='admin', **kwargs)


def match_schema(schema_class):
    """
    Matches request POST agains given schema
    """
    schema = schema_class()

    def decorator(fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            self = args[0]
            request = self.request
            if request.method not in ['POST', 'PUT']:
                return fn(*args, **kwargs)

            try:
                obj = schema.deserialize(request.POST)
                return fn(*args, obj=obj, **kwargs)
            except colander.Invalid as ci:
                return HTTPBadRequest(str(ci))
        return wrapper
    return decorator


def prepared_cmd(fn):
    """
    Used as a base for CLI commands
    """
    wrapped_fn = click.argument('config-path')(fn)

    @wraps(fn)
    def wrapper(**kwargs):
        load_dotenv(find_dotenv())
        config_path = kwargs.get('config_path')
        setup_logging(config_path)
        settings = get_appsettings(config_path)
        del kwargs['config_path']
        return wrapped_fn(settings=settings, **kwargs)
    return wrapper

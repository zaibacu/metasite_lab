from colander import (MappingSchema,
                      String,
                      Email,
                      Length,
                      Boolean,
                      SchemaType,
                      null,
                      Invalid,
                      SchemaNode)


class TopicSchema(MappingSchema):
    title = SchemaNode(String(allow_empty=False))
    active = SchemaNode(Boolean(), missing=False)


class StringList(SchemaType):
    def deserialize(self, node, cstruct):
        if cstruct is null:
            return null
        if not isinstance(cstruct, basestring):
            raise Invalid(node, '{0} is not a string'.format(cstruct))

        value = cstruct.split(',')
        return value


class SubscriberSchema(MappingSchema):
    name = SchemaNode(String(allow_empty=False),
                      validator=Length(min=3))
    email = SchemaNode(String(allow_empty=False),
                       validator=Email())

    topics = SchemaNode(StringList(),
                        validator=Length(min=1))


class SubscriberLiteSchema(MappingSchema):
    name = SchemaNode(String(allow_empty=False),
                      validator=Length(min=3))
    email = SchemaNode(String(allow_empty=False),
                       validator=Email())

import os

from pyramid.paster import get_app, setup_logging
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

config_path = os.getenv('CONFIG_PATH', 'config/development.ini')

setup_logging(config_path)

application = get_app(config_path)

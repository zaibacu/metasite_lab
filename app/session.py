import os
from pyramid.session import SignedCookieSessionFactory

session_factory = SignedCookieSessionFactory(os.getenv('SECRET_KEY'))


def includeme(config):
    config.set_session_factory(session_factory)

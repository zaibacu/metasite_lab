"""
    Created using:
    https://docs.pylonsproject.org/projects/pyramid-cookbook/en/latest/database/sqlalchemy.html
"""
import click
import transaction

from sqlalchemy import engine_from_config
from sqlalchemy.orm import scoped_session, sessionmaker

from app.models import Base
from app.models.user import User
from app.utils import prepared_cmd


DBSession = scoped_session(sessionmaker())


def init_sql(engine):
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)


def db(request):
    maker = request.registry.dbmaker
    session = maker()

    def cleanup(request):
        try:
            if request.exception is not None:
                session.rollback()
            else:
                session.commit()
        finally:
            session.close()

    request.add_finished_callback(cleanup)
    return session


def includeme(config):
    settings = config.registry.settings
    engine = engine_from_config(settings, prefix='sqlalchemy.')
    config.registry.dbmaker = sessionmaker(bind=engine)
    config.add_request_method(db, 'dbsession', reify=True)
    init_sql(engine)


@click.group()
def cli():
    pass


@cli.command('init')
@prepared_cmd
def init(settings):
    engine = engine_from_config(settings, prefix='sqlalchemy.')
    init_sql(engine)


@cli.command('createsuperuser')
@prepared_cmd
def create_superuser(settings):
    from pyramid.config import Configurator
    config = Configurator(settings=settings)
    includeme(config)

    username = click.prompt('Enter username')
    email = click.prompt('Enter email')
    password = click.prompt('Enter password',
                            hide_input=True,
                            confirmation_prompt=True)

    with transaction.manager:
        user = User(username, email, password)
        DBSession.add(user)
    DBSession.commit()


if __name__ == '__main__':
    cli()

from pyramid.view import view_defaults, view_config
from pyramid.security import remember, forget
from pyramid.csrf import get_csrf_token
from pyramid.httpexceptions import HTTPFound
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm import joinedload
from sqlalchemy import desc, asc

from app.utils import admin_view, match_schema
from app.schemas import TopicSchema, SubscriberLiteSchema
from app.services.topic import TopicService
from app.models.user import User, hash_password
from app.models.topic import Topic
from app.models.subscriber import Subscriber


class TopicViews(object):

    def __init__(self, request):
        self.request = request
        self.default_url = self.request.route_path('admin_topics')

    @admin_view(route_name='admin_topics',
                renderer='admin/topics.html.j2')
    def list(self):
        session = self.request.dbsession
        topics = session.query(Topic).all()
        return {'topics': topics}

    @match_schema(TopicSchema)
    @admin_view(route_name='admin_topic_create',
                renderer='admin/topic_form.html.j2')
    def create(self, obj=None):
        if self.request.method == 'POST':
            session = self.request.dbsession
            topic = TopicService.create_from_schema(obj)
            session.add(topic)
            session.commit()
            return HTTPFound(location=self.default_url)
        else:
            context = {
                'csrf_token': get_csrf_token(self.request),
                'topic': None
            }
            return context

    @match_schema(TopicSchema)
    @admin_view(route_name='admin_topic_edit',
                renderer='admin/topic_form.html.j2')
    def update(self, obj=None):
        topic_id = self.request.matchdict['topic_id']
        session = self.request.dbsession
        topic = session.query(Topic).filter_by(id=topic_id).one()
        if self.request.method == 'POST':
            topic.title = obj['title']
            topic.active = obj['active']
            session.commit()
            return HTTPFound(location=self.default_url)
        else:
            context = {
                'csrf_token': get_csrf_token(self.request),
                'topic': topic
            }
            return context

    @admin_view(route_name='admin_topic_delete')
    def delete(self):
        topic_id = self.request.matchdict['topic_id']
        session = self.request.dbsession

        topic = (session.query(Topic)
                        .filter_by(id=topic_id)
                        .one())
        session.delete(topic)
        return HTTPFound(location=self.default_url)


class SubscriberViews(object):
    def __init__(self, request):
        self.request = request
        self.default_url = self.request.route_path('admin_subscribers')

    @admin_view(route_name='admin_subscribers',
                renderer='admin/subscribers.html.j2')
    def list(self):
        sort = self.request.GET.get('sort', 'id')
        if 'created_at' in sort:
            """
            DateTime object sorting works differently.
            Need to cover this corner case
            """
            if sort[0] == '-':
                sort = desc('created_at')
            else:
                sort = asc('created_at')

        session = self.request.dbsession
        subscribers = (session.query(Subscriber)
                              .options(joinedload(Subscriber.topics))
                              .order_by(sort)
                              .all())

        def sort_resolver(x):
            return (x
                    if self.request.GET.get('sort', None) != x
                    else '-{0}'.format(x))

        return {'subscribers': subscribers,
                'name_sort': sort_resolver('name'),
                'email_sort': sort_resolver('email'),
                'created_at_sort': sort_resolver('created_at')}

    @match_schema(SubscriberLiteSchema)
    @admin_view(route_name='admin_subscriber_edit',
                renderer='admin/subscriber_form.html.j2')
    def update(self, obj=None):
        subscriber_id = self.request.matchdict['subscriber_id']
        session = self.request.dbsession
        subscriber = (session.query(Subscriber)
                             .filter_by(id=subscriber_id)
                             .one())

        if self.request.method == 'POST':
            subscriber.name = obj['name']
            subscriber.email = obj['email']
            session.commit()
            return HTTPFound(location=self.default_url)
        else:
            context = {
                'csrf_token': get_csrf_token(self.request),
                'subscriber': subscriber
            }
            return context

    @admin_view(route_name='admin_subscriber_delete')
    def delete(self):
        subscriber_id = self.request.matchdict['subscriber_id']
        session = self.request.dbsession

        subscriber = (session.query(Subscriber)
                             .filter_by(id=subscriber_id)
                             .one())
        session.delete(subscriber)
        return HTTPFound(location=self.default_url)


@view_defaults()
class AdminViews(object):

    def __init__(self, request):
        self.request = request

    @view_config(route_name='admin_login',
                 renderer='admin/login.html.j2')
    def login_form(self):
        context = {
            'csrf_token': get_csrf_token(self.request)
        }
        return context

    @view_config(route_name='admin_login',
                 request_method='POST')
    def login(self):
        username = self.request.POST['username']
        password = hash_password(self.request.POST['password'])
        session = self.request.dbsession
        try:
            user = (session.query(User)
                           .filter_by(username=username, password=password)
                           .one())
        except NoResultFound:
            failure_url = self.request.route_path('admin_login')
            return HTTPFound(location=failure_url)

        success_url = self.request.route_path('admin_subscribers')
        headers = remember(self.request, user.id, max_age='86400')
        response = HTTPFound(location=success_url)
        response.headerlist.extend(headers)
        return response

    @view_config(route_name='admin_logout')
    def logout(self):
        redirect_url = self.request.route_path('subscribe')
        headers = forget(self.request)
        response = HTTPFound(location=redirect_url)
        response.headerlist.extend(headers)
        return response

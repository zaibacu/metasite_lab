from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound
from pyramid.csrf import get_csrf_token

from app.schemas import SubscriberSchema
from app.utils import match_schema
from app.models.topic import Topic
from app.services.subscriber import SubscriberService


@view_defaults(renderer='subscribe.html.j2')
class SubscribersView(object):

    def __init__(self, request):
        self.request = request

    @match_schema(SubscriberSchema)
    @view_config(route_name='subscribe')
    def home(self, obj=None):
        if self.request.method == 'POST':
            session = self.request.dbsession
            subscriber = SubscriberService.create_from_schema(obj, session)
            SubscriberService.save_object(subscriber, session)
            return HTTPFound(location=self.request.route_path('subscribe'))
        else:
            session = self.request.dbsession
            topics = session.query(Topic).filter_by(active=True)
            context = {
                'csrf_token': get_csrf_token(self.request),
                'topics': topics
            }
            return context

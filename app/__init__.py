VERSION = (1, 0, '1a1')


def get_version():
    return '.'.join(map(str, VERSION))


def main(global_config, **settings):
    from pyramid.config import Configurator
    config = Configurator(settings=settings)
    config.scan('app.models')
    config.scan('app.views')
    config.include('app.routes')
    config.include('app.db')
    config.include('app.session')
    config.include('app.security')
    config.include('pyramid_jinja2')
    config.add_jinja2_renderer('.j2')
    config.add_jinja2_search_path('app:templates', name='.j2')

    config.set_default_csrf_options(require_csrf=True)

    return config.make_wsgi_app()

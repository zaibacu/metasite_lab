def topics(config):
    config.add_route('admin_topics', '/')
    config.add_route('admin_topic_create', '/create')
    config.add_route('admin_topic_edit', '/{topic_id}/edit')
    config.add_route('admin_topic_delete', '/{topic_id}/delete')


def subscribers(config):
    config.add_route('admin_subscribers', '/')
    config.add_route('admin_subscriber_edit', '/{subscriber_id}/edit')
    config.add_route('admin_subscriber_delete', '/{subscriber_id}/delete')


def includeme(config):
    config.add_route('admin_login', '/login')
    config.add_route('admin_logout', '/logout')
    config.include('app.routes.admin.topics', route_prefix='/topics')
    config.include('app.routes.admin.subscribers', route_prefix='/subscribers')

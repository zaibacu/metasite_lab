def includeme(config):
    config.include('app.routes.common')
    config.include('app.routes.admin', route_prefix='/admin')

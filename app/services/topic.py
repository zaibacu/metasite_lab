from app.models.topic import Topic


class TopicService(object):

    @classmethod
    def create_from_schema(cls, schema):
        return Topic(schema['title'], schema['active'])

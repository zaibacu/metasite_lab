import transaction

from app.models.subscriber import Subscriber
from app.models.topic import Topic


class SubscriberService(object):

    @classmethod
    def create_from_schema(cls, schema, session):
        topics = (session.query(Topic)
                         .filter(Topic.id.in_(schema['topics']))
                         .all())
        return Subscriber(schema['name'], schema['email'], topics)

    @classmethod
    def save_object(cls, subscriber, session):
        with transaction.manager:
            session.add(subscriber)
            session.commit()

from pyramid.authentication import SessionAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.security import Allow, Authenticated, unauthenticated_userid

from app.models.user import User


class RootFactory(object):
    def __init__(self, request):
        self.__acl__ = [(Allow, Authenticated, 'admin',)]


def get_user(request):
    session = request.dbsession
    user_id = unauthenticated_userid(request)
    if user_id is not None:
        return session.query(User).filter_by(id=user_id).one()


def includeme(config):
    authentication_policy = SessionAuthenticationPolicy()
    authorization_policy = ACLAuthorizationPolicy()

    config.set_authentication_policy(authentication_policy)
    config.set_authorization_policy(authorization_policy)
    config.set_root_factory(RootFactory)
    config.add_request_method(get_user, 'user', reify=True)

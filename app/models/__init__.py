from datetime import datetime

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (Column, DateTime, Table, Integer, ForeignKey)


Base = declarative_base()


subscriber_topics = Table(
        'Subscriber_Topics',
        Base.metadata,
        Column('subscriber_id', Integer, ForeignKey('subscribers.id')),
        Column('topic_id', Integer, ForeignKey('topics.id')),
)


class TrackingMixin(object):
    created_at = Column(
            DateTime,
            default=datetime.utcnow,
            nullable=False
    )
    changed_at = Column(
            DateTime,
            default=datetime.utcnow,
            onupdate=datetime.utcnow,
            nullable=False
    )

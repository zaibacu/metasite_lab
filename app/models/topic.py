from sqlalchemy import (Column, Unicode, Integer, Boolean)
from sqlalchemy.orm import relationship

from app.models import Base, subscriber_topics


class Topic(Base):
    __tablename__ = 'topics'
    id = Column(Integer, primary_key=True)
    title = Column(Unicode(255))
    active = Column(Boolean, default=False)

    subscribers = relationship(
            'Subscriber',
            secondary=subscriber_topics,
            back_populates='topics'
    )

    def __init__(self, title, active=False):
        self.title = title
        self.active = active

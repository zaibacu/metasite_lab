import os

from sqlalchemy import (Column, Unicode, Integer)
from passlib.hash import pbkdf2_sha256

from app.models import Base


def hash_password(password, hash_fn=pbkdf2_sha256):
    secret = os.getenv('SECRET_KEY').encode('UTF-8')
    return hash_fn.using(salt=secret).hash(password)


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(Unicode(255), unique=True, nullable=False)
    email = Column(Unicode(255), unique=True, nullable=False)
    password = Column(Unicode(255), nullable=False)

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = hash_password(password)

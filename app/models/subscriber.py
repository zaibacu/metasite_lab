from sqlalchemy import (Column, Unicode, Integer)
from sqlalchemy.orm import relationship

from app.models import Base, TrackingMixin, subscriber_topics


class Subscriber(Base, TrackingMixin):
    __tablename__ = 'subscribers'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255), nullable=False)
    email = Column(Unicode(255), unique=True, nullable=False)
    topics = relationship(
            'Topic',
            secondary=subscriber_topics,
            back_populates='subscribers',
            lazy='joined'
    )

    def __init__(self, name, email, topics):
        self.name = name
        self.email = email
        for topic in topics:
            self.topics.append(topic)

About
======
Metasite Lab project

Env setup
----------
`.env` file should be created, currently it is required to have:
```bash
SECRET_KEY=<some_random_key>
```
Optionally `CONFIG_PATH` could be changed to select different config file

Startup using Docker
--------------------
`docker-compose up`

To rebuild

`docker-compose up --build`

App should be running on port `8500`, nginx on `8000`
(It is intended that you'll call via nginx)

Startup locally
---------------
`gunicorn app.wsgi --reload`

It will listen on `8000` port

Init scripts
-------------

Init database
`python -m app.db init <config_file_path>`

Create a super user
`python -m app.db createsuperuser <config_file_path>`

#!/usr/bin/python
# -*- coding: UTF-8 -*-
from setuptools import setup, find_packages
from pip.req import parse_requirements

from app import get_version

install_reqs = list(parse_requirements('requirements/base.txt',
                                       session={}))

test_reqs = list(parse_requirements('requirements/test.txt',
                                    session={}))

setup(
    name='metasite_lab',
    version=get_version(),
    author='Šarūnas Navickas',
    author_email='sarunas@navickas.info',
    packages=find_packages(),
    install_requires=[str(ir.req) for ir in install_reqs],
    tests_require=[str(tr.req) for tr in test_reqs],
    test_suite="pytest",
    setup_requires=["pytest-runner"],
    entry_points={
        'paste.app_factory': [
            'main = app:main'
        ],
    }
)
